package com.bank.loanms.repository;

import java.util.List;

import com.bank.loanms.model.Loan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LoanRepository extends CrudRepository<Loan, Long> {

	List<Loan> findByCustomerIdOrderByStartDtDesc(int customerId);

}