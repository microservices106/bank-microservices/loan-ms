package com.bank.loanms.controller;

import java.util.List;

import com.bank.loanms.config.LoanServiceConfig;
import com.bank.loanms.model.Customer;
import com.bank.loanms.model.Loan;
import com.bank.loanms.model.Properties;
import com.bank.loanms.repository.LoanRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@Log4j2

@RestController
@RequestMapping("loans")
@RequiredArgsConstructor
public class LoanController {

	private final LoanRepository loanRepository;
	private final LoanServiceConfig accountServiceConfig;


	@PostMapping
	public List<Loan> getLoansDetails(@RequestHeader("eazybank-correlation-id") String correlationId,
									  @RequestBody Customer customer) {
		log.info("getLoansDetails started");
		return loanRepository.findByCustomerIdOrderByStartDtDesc(customer.getCustomerId());
	}

	@GetMapping("properties")
	public Properties getPropertyDetails() {
		return Properties.builder()
				.msg(accountServiceConfig.getMsg())
				.buildVersion(accountServiceConfig.getBuildVersion())
				.mailDetails(accountServiceConfig.getMailDetails())
				.activeBranches(accountServiceConfig.getActiveBranches())
				.build();
	}

}